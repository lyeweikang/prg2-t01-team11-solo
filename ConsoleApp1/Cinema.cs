﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Cinema
    {
        public string Name { get; set; }
        public int HallNo { get; set; }
        public int Capacity { get; set; }
        public Cinema() { }
        public Cinema(string n, int h, int c)
        {
            Name = n;
            HallNo = h;
            Capacity = c;
        }
        public override string ToString()
        {
            return "\t Name: " + Name + "\t HallNo: " + HallNo + "\t Capacity: " + Capacity;
        }
    }
}
