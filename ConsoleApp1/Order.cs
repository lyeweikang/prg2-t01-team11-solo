﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Order
    {
        public int OrderNo { get; set; }
        public DateTime OrderDateTime { get; set; }
        public double Amount { get; set; }
        public string Status { get; set; }
        public List<Ticket> TicketList { get; set; }
                         = new List<Ticket>();
        public Order() { }
        public Order(int on, DateTime odt)
        {
            OrderNo = on;
            OrderDateTime = odt;
        }
        public void AddTicket(Ticket ticket)
        {
            TicketList.Add(ticket);
        }
        public override string ToString()
        {
            return "OrderNo: " + OrderNo + "\t OrderDateTime: " + OrderDateTime + "\t Amount: " + Amount
                + "\t Status" + Status;
        }
    }
}
