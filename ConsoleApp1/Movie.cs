﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Movie
    {
        public string Title { get; set; }
        public int Duration { get; set; }
        public string Classification { get; set; }
        public DateTime OpeningDate { get; set; }
        public List<string> GenreList { get; set; }= new List<string>();
        public List<Screening> ScreeningList { get; set; } = new List<Screening>();
        public Movie() { }
        public Movie(string t, int d, string c, DateTime o, List<string> g)
        {
            Title = t;
            Duration = d;
            Classification = c;
            OpeningDate = o;
            GenreList = g;
        }
        public void AddGenre(string genre)
        {
            GenreList.Add(genre);
        }
        public void AddScreening(Screening screening)
        {
            ScreeningList.Add(screening);
        }
        public override string ToString()
        {
            return "Title: " + Title + "\t Duration: " + Duration + "\t Classification: " + Classification +
                "\t OpeningDate: " + OpeningDate.ToString("dd/MM/yyyy");
        }
    }
}
