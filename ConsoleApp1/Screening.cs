﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Screening:IComparable<Screening>
    {
        public int ScreeningNo { get; set; }
        public DateTime ScreeningDateTime { get; set; }
        public string ScreeningType { get; set; }
        public int SeatsRemaining { get; set; }
        public Cinema Cinema { get; set; }
        public Movie Movie { get; set; }

        public Screening() { }

        public Screening(int sn, DateTime sdt, string st, int sr, Cinema c, Movie m)
        {
            ScreeningNo = sn;
            ScreeningDateTime = sdt;
            ScreeningType = st;
            SeatsRemaining = sr;
            Cinema = c;
            Movie = m;
        }
        public int CompareTo(Screening screening)
        {
            if (SeatsRemaining <screening.SeatsRemaining)
                return 1;
            else if (SeatsRemaining == screening.SeatsRemaining)
                return 0;
            else
            {
                return -1;
            }
        }
        public override string ToString()
        {
            return "ScreeningNo: " + ScreeningNo + "\t ScreeningDateTime: " + ScreeningDateTime + "\t Screening Type: " + ScreeningType +
                "\t SeatsRemaning: " + SeatsRemaining + "\t Cinema: " + Cinema + "\t Movie:" + Movie;
        }
    }
}
