﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Adult:Ticket
    {
        public bool PopcornOffer { get; set; }
        public Adult() : base() { }
        public Adult(Screening s, bool p) : base(s)
        {
            PopcornOffer = p;
        }
        public override double CalculatePrice()
        {
            double price;
            string day = String.Format("{0:D}", Screening.ScreeningDateTime);  // "Sunday, March 09, 2008"
            string[] dayarray = day.Split(",");
            if (Screening.ScreeningType == "3D")
            {
                if (dayarray[0] == "Monday" || dayarray[0] == "Tuesday" || dayarray[0] == "Wednesday" || dayarray[0] == "Thursday")
                {
                    price = 11.00;
                    return price;
                }
                else
                {
                    price = 14.00;
                    return price;
                }
            }
            else
            {
                if (dayarray[0] == "Monday" || dayarray[0] == "Tuesday" || dayarray[0] == "Wednesday" || dayarray[0] == "Thursday")
                {
                    price = 8.50;
                }
                else
                {
                    price = 12.50;
                }
            }
            return price;
        }
        public override string ToString()
        {
            return base.ToString() + "\t PopcornOffer: " + PopcornOffer;
        }
    }
}
