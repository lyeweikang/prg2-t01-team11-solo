﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Student:Ticket
    {
        public string LevelOfStudy { get; set; }
        public Student() : base() { }
        public Student(Screening s, string l) : base(s)
        {
            LevelOfStudy = l;
        }
        public override double CalculatePrice()
        {
            double price;
            string day = String.Format("{0:D}", Screening.ScreeningDateTime);  // "Sunday, March 09, 2008"
            string[] dayarray = day.Split(",");
            if (Screening.ScreeningType == "3D")
            {
                if (Screening.ScreeningDateTime.Subtract(Screening.Movie.OpeningDate).Days <7)
                {
                    if (dayarray[0] == "Monday" || dayarray[0] == "Tuesday" || dayarray[0] == "Wednesday" || dayarray[0] == "Thursday")
                    {
                        price = 11.00;
                    }
                    else
                    {
                        price = 14.00;
                    }
                }
                else
                {
                    if (dayarray[0] == "Monday" || dayarray[0] == "Tuesday" || dayarray[0] == "Wednesday" || dayarray[0] == "Thursday")
                    {
                        price = 8.00;
                    }
                    else
                    {
                        price = 14.00;
                    }
                }
            }
            else
            {
                if (Screening.ScreeningDateTime.Subtract(Screening.Movie.OpeningDate).Days <7)
                {
                    if (dayarray[0] == "Monday" || dayarray[0] == "Tuesday" || dayarray[0] == "Wednesday" || dayarray[0] == "Thursday")
                    {
                        price = 8.50;
                    }
                    else
                    {
                        price = 12.50;
                    }
                }
                else
                {
                    if (dayarray[0] == "Monday" || dayarray[0] == "Tuesday" || dayarray[0] == "Wednesday" || dayarray[0] == "Thursday")
                    {
                        price = 7.00;
                    }
                    else
                    {
                        price = 12.50;
                    }

                }
            }
            return price;
        }
        public override string ToString()
        {
            return base.ToString() + "\t LevelOfStudy: " + LevelOfStudy;
        }
    }
}
