﻿using System;
using System.Collections.Generic;
using System.IO;

namespace ConsoleApp1
{
    class Program
    {
        static int screeningNo = 1001;
        static int orderNo = 1;
        static void Main(string[] args)
        {
            List<Movie> movieList = new List<Movie>();
            List<Cinema> cinemaList = new List<Cinema>();
            List<Screening> screeningList = new List<Screening>();
            List<Order> orderList = new List<Order>();
            bool loadMCData = false;
            bool loadSData = false;
            while (true)
            {
                try
                {
                    Console.WriteLine("");
                    Displaymenu();
                    Console.WriteLine("");
                    Console.Write("Enter option: ");
                    int option = Convert.ToInt32(Console.ReadLine());
                    if (option == 1)
                    {
                        if (loadMCData == false)
                        {
                            LoadMCData(movieList, cinemaList);
                            loadMCData = true;
                        }
                        else
                        {
                            Console.WriteLine("The Movie and Cinema data has already been loaded and cant be loaded again.");
                        }
                    }
                    else if (option == 2)
                    {
                        if (loadSData == false)
                        {
                            LoadSData(screeningList, cinemaList, movieList);
                            loadSData = true;
                        }
                        else
                        {
                            Console.WriteLine("The Screening data has already been loaded and cant be loaded again.");
                        }
                    }
                    else if (option == 3)
                    {
                        if (loadMCData==true && loadSData == true)
                        {
                            DisplayMovieInformation(movieList);
                        }
                        else
                        {
                            Console.WriteLine("Please load the required data before using this feature.");
                        }
                    }
                    //4 List movie screenings
                    else if (option == 4)
                    {
                        if (loadMCData == true && loadSData == true)
                        {
                            ListMovieScreenings(movieList);
                        }
                        else
                        {
                            Console.WriteLine("Please load the required data before using this feature.");
                        }
                    }
                    //5 Add a movie screening session
                    else if (option == 5)
                    {
                        if (loadMCData == true && loadSData == true)
                        {
                            AddMovieScreeningSession(movieList, screeningList, cinemaList);
                        }
                        else
                        {
                            Console.WriteLine("Please load the required data before using this feature.");
                        }
                      
                    }
                    //6 Delete a movie screening session
                    else if (option == 6)
                    {
                        if (loadMCData == true && loadSData == true)
                        {
                            DeleteMovieSession(movieList, screeningList);
                        }
                        else
                        {
                            Console.WriteLine("Please load the required data before using this feature.");
                        }
                    }
                    else if (option == 7)
                    {
                        if (loadMCData == true && loadSData == true)
                        {
                            OrderMovieTickets(movieList, orderList,screeningList);
                        }
                        else
                        {
                            Console.WriteLine("Please load the required data before using this feature.");
                        }
                    }
                    else if (option == 8)
                    {
                        if (loadMCData == true && loadSData == true)
                        {
                            DisplayOrder(orderList);
                            CancelOrder(movieList, orderList, screeningList);
                        }
                        else
                        {
                            Console.WriteLine("Please load the required data before using this feature.");
                        }
                    }
                    else if (option == 9)
                    {
                        if (loadMCData == true && loadSData == true)
                        {
                            RecommendMovie(movieList, orderList);
                        }
                        else
                        {
                            Console.WriteLine("Please load the required data before using this feature.");
                        }
                    }
                    else if (option == 10)
                    {
                        if (loadMCData == true && loadSData == true)
                        {
                            DisplayAvailableSeats(screeningList);
                        }
                        else
                        {
                            Console.WriteLine("Please load the required data before using this feature.");
                        }
                    }
                    else if (option == 0)
                    {
                        Console.WriteLine("Bye!");
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Please enter a option between 0 to 10.");
                    }
                }
                catch (FormatException)
                {
                    Console.WriteLine("Please enter a option made up of integer that is between 0 to 10.");
                }
                catch (OverflowException)
                {
                    Console.WriteLine("Please etner a option that is made up of one integer that is between 0 to 10. ");
                }
            }


        }

        //MainMenu
        static void Displaymenu()
        {
            Console.WriteLine("---------Singa Cineplexes---------");
            Console.WriteLine("[0] Exit");
            Console.WriteLine("[1] Load Movie and Cinema Data");
            Console.WriteLine("[2] Load Screening Data");
            Console.WriteLine("[3] List all movies");
            Console.WriteLine("[4] List movie screenings");
            Console.WriteLine("[5] Add a movie screening session");
            Console.WriteLine("[6] Delete a movie screening session");
            Console.WriteLine("[7] Order movie ticket/s");
            Console.WriteLine("[8] Cancel order of ticket");
            Console.WriteLine("[9] Recommend a movie");
            Console.WriteLine("[10] Display available seats of screening session in descending order");
            Console.WriteLine("---------------------------------");
        }

        //1 Load Movie and Cinema data
        static void LoadMCData(List<Movie> movieList, List<Cinema> cinemaList)
        {
            string[] csvlines = File.ReadAllLines("Movie.csv");
            for (int i = 1; i < csvlines.Length; i++)
            {
                string[] dataMovie = csvlines[i].Split(",");
                List<string> genreList = new List<string>();
                if (dataMovie[2].Contains("/"))
                {
                    string[] genreArray = dataMovie[2].Split("/");
                    foreach (string genre in genreArray)
                    {
                        genreList.Add(genre);
                    }
                }
                else
                {
                    genreList.Add(dataMovie[2]);
                }
                if (dataMovie[4].Contains("/"))
                {
                    string[] date = dataMovie[4].Split("/");
                    DateTime datetime = Convert.ToDateTime(date[2] + "/" + date[1] + "/" + date[0]);
                    movieList.Add(new Movie(dataMovie[0], Convert.ToInt32(dataMovie[1]), dataMovie[3], datetime, genreList));
                }

            }
            //Load Cinema Data
            string[] csvlines2 = File.ReadAllLines("Cinema.csv");
            for (int i = 1; i < csvlines2.Length; i++)
            {
                string[] dataCinema = csvlines2[i].Split(",");
                cinemaList.Add(new Cinema(dataCinema[0], Convert.ToInt32(dataCinema[1]), Convert.ToInt32(dataCinema[2])));
            }
            Console.WriteLine("Movie and cinema data has been loaded successfully.");

        }
        //2 Load Screening Data
        static void LoadSData(List<Screening> screeningList,List<Cinema> cinemaList,List<Movie> movieList)
        {
            string[] csvlines3 = File.ReadAllLines("Screening.csv");
            for (int i = 1; i < csvlines3.Length; i++)
            {
                string[] dataScreening = csvlines3[i].Split(",");
                Cinema screeningCinema = new Cinema();
                Screening screening = new Screening();
                foreach (Cinema cinema in cinemaList)
                {
                    if (cinema.Name == dataScreening[2] && cinema.HallNo == Convert.ToInt32(dataScreening[3]))
                    {
                        screeningCinema = cinema;
                    }
                }
                foreach (Movie movie in movieList)
                {
                    if (movie.Title == dataScreening[4])
                    {
                        screening = new Screening(screeningNo, Convert.ToDateTime(dataScreening[0]), dataScreening[1], screeningCinema.Capacity, screeningCinema, movie);
                        movie.AddScreening(screening);
                    }
                }
                screeningList.Add(screening);
                screeningNo += 1;
            }
            Console.WriteLine("Screening data has been successfully loaded.");

        }
        //3 Display Movie information
        static void DisplayMovieInformation(List<Movie> movieList)
        {
            Console.WriteLine("");
            Console.WriteLine("{0,-25}{1,-20}{2,-20}{3,-20}{4,-20}", "Title", "Duration", "Classification", "Opening Date", "Genres");
            for (int s = 0; s < movieList.Count; s++)
            {
                string genres = "";
                foreach (string genre in movieList[s].GenreList)
                {
                    if (movieList[s].GenreList.Count != 1 && movieList[s].GenreList.IndexOf(genre) != 0)
                    {
                        genres += "/" + genre;
                    }
                    else
                    {
                        genres += genre;
                    }
                }
                Console.WriteLine("{0,-25}{1,-20}{2,-20}{3,-20}{4,-20}", movieList[s].Title, movieList[s].Duration, movieList[s].Classification, movieList[s].OpeningDate.ToString("dd/MM/yyyy"), genres);
            }

        }

        //4 List Movie Screenings
        static Movie ListMovieScreenings(List<Movie> movieList)
        {
            DisplayMovieInformation(movieList);
            Console.WriteLine("");
            Movie selectedMovie = new Movie();
            while (true)
            {
                Console.Write("Select a movie to watch: ");
                string movieTitle = Console.ReadLine();
                bool validMovie = false;
                foreach (Movie movie in movieList)
                {
                    if (movie.Title.ToUpper() == movieTitle.ToUpper())
                    {
                        selectedMovie = movie;
                        validMovie = true;
                    }
                }
                if (validMovie == true)
                {
                    Console.WriteLine("{0,-15}{1,-30}{2,-15}{3,-15}{4,-15}{5,-15}", "ScreeningNo", "ScreeningDateTime", "ScreeningType", "SeatsRemaining","CinemaName","CinemaHallNo");
                    foreach (Screening screening in selectedMovie.ScreeningList)
                    {
                        Console.WriteLine("{0,-15}{1,-30}{2,-15}{3,-15}{4,-15}{5,-15}", screening.ScreeningNo, screening.ScreeningDateTime, screening.ScreeningType, screening.SeatsRemaining,screening.Cinema.Name,screening.Cinema.HallNo);
                    }
                    break;
                }
                else
                {
                    Console.WriteLine("Please enter a valid movie that is listed above.");
                }
            }
            return selectedMovie;

        }
        //5 Add a movie screening session
        static void AddMovieScreeningSession(List<Movie> movieList,List<Screening>screeningList,List<Cinema> cinemaList)
        {
            Console.WriteLine("");
            DisplayMovieInformation(movieList);
            Console.WriteLine("");
            while (true)
            {
                Console.Write("Select a movie to watch: ");
                string movieTitle = Console.ReadLine();
                bool validMovie = false;
                Movie selectedMovie = new Movie();
                foreach (Movie movie in movieList)
                {
                    if (movie.Title.ToUpper() == movieTitle.ToUpper())
                    {
                        selectedMovie = movie;
                        validMovie = true;
                    }

                }
                if (validMovie == true)
                {
                    string screeningType;
                    DateTime screeningDateTime;
                    while (true)
                    {
                        Console.Write("Enter a screening type[2D/3D]: ");
                        screeningType = Console.ReadLine().ToUpper();
                        if (screeningType == "3D" || screeningType == "2D")
                        {
                            break;
                        }
                        else
                        {
                            Console.WriteLine("Please enter a valid screening type.");
                        }
                    }
                    while (true)
                    {
                        try
                        {
                            Console.Write("Enter a screening date and time[yyyy/MM/dd ##:##]: ");
                            screeningDateTime = Convert.ToDateTime(Console.ReadLine());
                            break;
                        }
                        catch (FormatException)
                        {
                            Console.WriteLine("Please enter the screening date and time in a correct format[yyyy/MM/dd ##:##] and also ensure that the year/month/day and time entered is valid.");
                        }
                        
                    }
                    bool validCinema = false;
                    bool validHallNo = false;
                    string cinemaName;
                    int hallNo;
                    //Check if screening DateTime is after movie opening DateTime
                    if (screeningDateTime.Subtract(selectedMovie.OpeningDate).Days >= 0)
                    {
                        Console.WriteLine("{0,-15}{1,-15}{2,-15}", "CinemaName", "CinemaHallNo", "CinemaCapacity");
                        foreach (Cinema cinema in cinemaList)
                        {
                            Console.WriteLine("{0,-15}{1,-15}{2,-15}", cinema.Name, cinema.HallNo, cinema.Capacity);
                        }
                        while (true)
                        {
                            Console.Write("Enter a cinema name: ");
                            cinemaName = Console.ReadLine();
                            foreach (Cinema cinema in cinemaList)
                            {
                                if (cinemaName.ToUpper() == cinema.Name.ToUpper())
                                {
                                    validCinema = true;
                                }

                            }
                            if (validCinema == true)
                            {
                                break;
                            }
                            else
                            {
                                Console.WriteLine("Please enter a valid cinema name.");
                            }
                        }
                        while (true)
                        {
                            try
                            {
                                Console.Write("Enter a cinema hall no: ");
                                hallNo = Convert.ToInt32(Console.ReadLine());
                                foreach (Cinema cinema in cinemaList)
                                {
                                    if (cinemaName.ToUpper() == cinema.Name.ToUpper())
                                    {
                                        if (cinema.HallNo == hallNo)
                                        {
                                            validHallNo = true;
                                        } 
                                    }

                                }
                                if (validHallNo == true)
                                {
                                    break;
                                }
                                else
                                {
                                    Console.WriteLine("Please enter a valid hallno that is listed above.");
                                }
                            }
                            catch (FormatException)
                            {
                                Console.WriteLine("Please enter a valid hallNo made up of integer.");
                            }
                            catch (OverflowException)
                            {
                                Console.WriteLine("Please enter a valid hallNo that is made up of one integer.");
                            }
                            
                        }
                        Console.WriteLine("");
                        Console.WriteLine($"Screening on {screeningDateTime.Date.ToString("dd/MM/yyyy")} at {cinemaName} hall number {hallNo}");
                        Console.WriteLine("{0,-15}{1,-30}{2,-15}{3,-15}{4,-15}{5,-15}", "ScreeningNo", "ScreeningDateTime", "ScreeningType", "SeatsRemaining", "CinemaName", "CinemaHallNo");
                        foreach (Screening screening in screeningList)
                        {
                            if (screening.ScreeningDateTime.Date==screeningDateTime.Date&&screening.Cinema.Name.ToUpper()==cinemaName.ToUpper()&& screening.Cinema.HallNo == hallNo)
                            {
                                Console.WriteLine("{0,-15}{1,-30}{2,-15}{3,-15}{4,-15}{5,-15}", screening.ScreeningNo, screening.ScreeningDateTime, screening.ScreeningType, screening.SeatsRemaining, screening.Cinema.Name, screening.Cinema.HallNo);
                                
                            }
                        }
                        Console.WriteLine("");
                        //Checking whether hall is available
                        bool status = true;
                        foreach (Screening screening in screeningList)
                        {
                            if (screening.Cinema.HallNo == hallNo && screening.Cinema.Name.ToUpper() == cinemaName.ToUpper())
                            {
                                if (screening.ScreeningDateTime.Date == screeningDateTime.Date)
                                {
                                    status = CheckMovieTiming(screening, screeningDateTime, selectedMovie, status);
                                }

                            }
                        }
                        Cinema selectedCinema = new Cinema();
                        if (status == true)
                        {
                            foreach (Cinema cinema in cinemaList)
                            {
                                if (hallNo == cinema.HallNo && cinemaName.ToUpper() == cinema.Name.ToUpper())
                                {
                                    selectedCinema = cinema;
                                }
                            }
                            Screening newScreening = new Screening(screeningNo, screeningDateTime, screeningType, selectedCinema.Capacity, selectedCinema, selectedMovie);
                            screeningList.Add(newScreening);
                            selectedMovie.AddScreening(newScreening);
                            screeningNo += 1;
                            Console.WriteLine("New screening has successfully be created.");
                            break;
                        }
                        else
                        {
                            Console.WriteLine("The creation of new screening is unsuccessful.");
                            break;
                        }
                    }
                    else
                    {
                        Console.WriteLine("Please enter a screening datetime after the movie opening datetime.");
                        Console.WriteLine("The creation of new screening is unsuccessful.");
                        break;
                    }
                }
                else
                {
                    Console.WriteLine("Please enter a valid movie that is listed above.");
                }

            }
            

        }
            
        static bool CheckMovieTiming(Screening screening,DateTime screeningDateTime,Movie selectedMovie,bool status)
        {
            DateTime screeningEndTime;
            if (screening.ScreeningDateTime.TimeOfDay == screeningDateTime.TimeOfDay)
            {
                status = false;
            }
            else if (screeningDateTime.TimeOfDay>screening.ScreeningDateTime.TimeOfDay)
            {
                screeningEndTime = screening.ScreeningDateTime.AddMinutes(30 + screening.Movie.Duration);
                if (screeningEndTime.TimeOfDay > screeningDateTime.TimeOfDay)
                {
                    status = false;
                }
            }
            else if(screeningDateTime.TimeOfDay<screening.ScreeningDateTime.TimeOfDay)
            {
                screeningEndTime = screeningDateTime.AddMinutes(selectedMovie.Duration + 30);
                if (screeningEndTime.TimeOfDay > screening.ScreeningDateTime.TimeOfDay)
                {
                    status = false;
                }
            }
            return status;
        }

        //6 Deleted a movie screening session
        static void DeleteMovieSession(List<Movie> movieList, List<Screening> screeningList)
        {
            foreach (Movie movies in movieList)
            {
                Console.WriteLine("");
                Console.WriteLine(movies.Title);
                Console.WriteLine("{0,-15}{1,-30}{2,-15}{3,-15}{4,-15}{5,-15}", "ScreeningNo", "ScreeningDateTime", "ScreeningType", "SeatsRemaining","CinemaName","HallNo");
                foreach (Screening screenings in movies.ScreeningList)
                {
                    //Checking if there are no seats being sold
                    if (screenings.SeatsRemaining == screenings.Cinema.Capacity)
                    {
                        Console.WriteLine("{0,-15}{1,-30}{2,-15}{3,-15}{4,-15}{5,-15}", screenings.ScreeningNo, screenings.ScreeningDateTime, screenings.ScreeningType, screenings.SeatsRemaining,screenings.Cinema.Name,screenings.Cinema.HallNo);
                    }
                }
            }
    
            bool status = false;
            bool validScreeningNo = false;
            int screeningNo;
            while (true)
            {
                try 
                {
                    Console.Write("Enter a session to delete: ");
                    screeningNo = Convert.ToInt32(Console.ReadLine());
                    foreach (Screening screenings in screeningList)
                    {
                        if (screenings.ScreeningNo == screeningNo)
                        {
                            validScreeningNo = true;

                        }
                    }
                    if (validScreeningNo == true)
                    {
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Please enter a valid screening no that is listed above.");
                    }
                }
                catch (FormatException)
                {
                    Console.WriteLine("Please enter a valid screening no. that is made up of integers.");
                }
                catch(OverflowException)
                {
                    Console.WriteLine("Please enter a valid screening no. that is made up of 4 integers.");
                }
                
            }
            Screening screening = new Screening();
            Movie movie = new Movie();
            foreach (Movie movies in movieList)
            {
                foreach (Screening screenings in movies.ScreeningList)
                {
                    if (screeningNo == screenings.ScreeningNo)
                    {
                        movie = movies;
                    }
                }
            }
            foreach (Screening screenings in screeningList)
            {
                if (screenings.ScreeningNo == screeningNo)
                {
                    screening = screenings;
                }
            }
            if (screening.SeatsRemaining == screening.Cinema.Capacity)
            {
                status = true;   
            }
            if (status == true)
            {
                screeningList.Remove(screening);
                movie.ScreeningList.Remove(screening);
                Console.WriteLine("The deletion of session is successful.");
            }
            else
            {
                Console.WriteLine("The deletion of session is unsuccessful");
            }
        }
        //7 Order movie ticket/s
        static void OrderMovieTickets(List<Movie> movieList, List<Order> orderList, List<Screening> screeningList)
        {
            int screeningNo;
            Movie selectedMovie=ListMovieScreenings(movieList);
            bool gotScreening = false;
            foreach(Screening screenings in screeningList)
            {
                if (screenings.Movie.Title ==selectedMovie.Title)
                {
                    gotScreening = true;
                }
            }
            if (gotScreening == true)
            {
                while (true)
                {
                    try
                    {
                        Console.Write("Select a movie screening: ");
                        screeningNo = Convert.ToInt32(Console.ReadLine());
                        bool validScreeningNo = false;
                        Screening screening_s = new Screening();
                        foreach (Screening screenings in screeningList)
                        {
                            if (screenings.ScreeningNo == screeningNo)
                            {
                                screening_s = screenings;
                                validScreeningNo = true;
                            }
                        }
                        if (validScreeningNo == true)
                        {
                            if (screening_s.Movie.Title == selectedMovie.Title)
                            {
                                break;
                            }
                            else
                            {
                                Console.WriteLine($"Please enter a screening that screen the movie named {selectedMovie.Title}.");
                            }
                        }
                        else
                        {
                            Console.WriteLine("Please enter a valid screeningNo.");

                        }

                    }
                    catch (FormatException)
                    {
                        Console.WriteLine("Please enter a valid screening no. that is made up of integers.");
                    }
                    catch (OverflowException)
                    {
                        Console.WriteLine("Please enter a valid screening no. that is made up of 4 integers.");
                    }
                }

                Screening screening = new Screening();
                foreach (Movie movie in movieList)
                {
                    foreach (Screening screenings in movie.ScreeningList)
                    {
                        if (screenings.ScreeningNo == screeningNo)
                        {
                            screening = screenings;
                        }
                    }
                }
                int ticketsNo;
                while (true)
                {
                    try
                    {
                        Console.Write("Enter total number of tickets to order: ");
                        ticketsNo = Convert.ToInt32(Console.ReadLine());
                        if (ticketsNo != 0)
                        {
                            break;
                        }
                        else if (ticketsNo == 0)
                        {
                            Console.WriteLine("The minimum number of ticket to order is 1.");
                        }
                    }
                    catch (FormatException)
                    {
                        Console.WriteLine("Please enter a valid ticket no.");
                    }
                    catch (OverflowException)
                    {
                        Console.WriteLine("Please enter a valid ticket no.");
                    }
                }
                if (ticketsNo > screening.SeatsRemaining)
                {
                    Console.WriteLine("The number of tickets ordered is more than the seats available.");
                }
                else
                {
                    if (screening.Movie.Classification != "G")
                    {
                        bool requirementMet;
                        while (true)
                        {
                            try
                            {
                                Console.Write("Did all ticket holders meet the movie classification required except movies classified as G (true/false)? ");
                                requirementMet = Convert.ToBoolean(Console.ReadLine());
                                break;
                            }

                            catch (FormatException)
                            {
                                Console.WriteLine("Please enter true or false only");
                            }

                        }
                        if (requirementMet)
                        {
                            Order order = new Order(orderNo, DateTime.Now);
                            order.Status = "Unpaid";
                            orderNo += 1;
                            int studentTicketsNo = 0;
                            int seniorCitizenTicketsNo = 0;
                            int adultTicketsNo = 0;
                            while (true)
                            {
                                try
                                {
                                    Console.Write("Enter the total number of student tickets to order: ");
                                    studentTicketsNo = Convert.ToInt32(Console.ReadLine());
                                    break;
                                }
                                catch (FormatException)
                                {
                                    Console.WriteLine("Please enter a valid ticket no.");
                                }
                                catch (OverflowException)
                                {
                                    Console.WriteLine("Please enter a valid ticket no.");
                                }
                            }
                            if (studentTicketsNo >= 1 && studentTicketsNo <= ticketsNo)
                            {
                                for (int i = 1; i <= studentTicketsNo; i++)
                                {
                                    string levelOfStudy;
                                    if (screening.Movie.Classification == "PG13")
                                    {
                                        while (true)
                                        {
                                            Console.Write($"{i}. Level of study[Primary,Secondary,Tertiary]: ");
                                            levelOfStudy = Console.ReadLine();
                                            if (levelOfStudy.ToLower() == "primary" || levelOfStudy.ToLower() == "secondary" || levelOfStudy.ToLower() == "tertiary")
                                            {
                                                break;
                                            }
                                            else
                                            {
                                                Console.WriteLine("Please enter a valid level of study.");
                                            }

                                        }
                                        Student studentTicket = new Student(screening, levelOfStudy);
                                        order.AddTicket(studentTicket);
                                    }
                                    else if (screening.Movie.Classification == "NC16" || screening.Movie.Classification == "M18")
                                    {
                                        while (true)
                                        {
                                            Console.Write($"{i}. Level of study[Secondary,Tertiary]: ");
                                            levelOfStudy = Console.ReadLine();
                                            if (levelOfStudy.ToLower() == "secondary" || levelOfStudy.ToLower() == "tertiary")
                                            {
                                                break;
                                            }
                                            else
                                            {
                                                Console.WriteLine("Please enter a valid level of study.");
                                            }

                                        }
                                        Student studentTicket = new Student(screening, levelOfStudy);
                                        order.AddTicket(studentTicket);

                                    }
                                    else if (screening.Movie.Classification == "R21")
                                    {
                                        levelOfStudy = "Tertiary";
                                        Student studentTicket = new Student(screening, levelOfStudy);
                                        order.AddTicket(studentTicket);
                                    }
                                }
                            }
                            if (ticketsNo - studentTicketsNo > 0 && ticketsNo - studentTicketsNo != 0)
                            {
                                while (true)
                                {
                                    try
                                    {
                                        Console.Write("Enter the total number of senior citizen tickets to order: ");
                                        seniorCitizenTicketsNo = Convert.ToInt32(Console.ReadLine());
                                        break;
                                    }
                                    catch (FormatException)
                                    {
                                        Console.WriteLine("Please enter a valid ticket no.");
                                    }
                                    catch (OverflowException)
                                    {
                                        Console.WriteLine("Please enter a valid ticket no.");
                                    }
                                }
                                if (seniorCitizenTicketsNo >= 1 && seniorCitizenTicketsNo <= ticketsNo - studentTicketsNo)
                                {
                                    int yearOfBirth;
                                    for (int t = 1; t <= seniorCitizenTicketsNo; t++)
                                    {
                                        while (true)
                                        {
                                            try
                                            {
                                                Console.Write($"{t}.Year Of Birth: ");
                                                yearOfBirth = Convert.ToInt32(Console.ReadLine());
                                                if (yearOfBirth.ToString().Length == 4)
                                                {
                                                    if (DateTime.Now.Year - yearOfBirth >= 55)
                                                    {
                                                        SeniorCitizen seniorCitizenTicket = new SeniorCitizen(screening, yearOfBirth);
                                                        order.AddTicket(seniorCitizenTicket);
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        Console.WriteLine("The ticket holder must be 55 and above to buy seniorcitizen ticket. ");

                                                    }
                                                }
                                                else
                                                {
                                                    Console.WriteLine("Please enter a valid year made up of 4 integers.");
                                                }
                                            }
                                            catch (FormatException)
                                            {
                                                Console.WriteLine("Please enter a valid year made up of 4 integers.");
                                            }
                                            catch (OverflowException)
                                            {
                                                Console.WriteLine("Please enter a valid year made up of 4 integers.");
                                            }
                                        }

                                    }
                                }

                            }
                            if (ticketsNo - studentTicketsNo - seniorCitizenTicketsNo != 0 && ticketsNo - studentTicketsNo - seniorCitizenTicketsNo > 0)
                            {
                                adultTicketsNo = ticketsNo - studentTicketsNo - seniorCitizenTicketsNo;
                                Console.WriteLine($"Number of adult tickets : {adultTicketsNo}");
                                string popcornResponse;
                                for (int l = 1; l <= adultTicketsNo; l++)
                                {
                                    while (true)
                                    {
                                        Console.Write($"{l}. Popcorn for $3 [Y/N]: ");
                                        popcornResponse = Console.ReadLine();
                                        if (popcornResponse.ToUpper() == "Y" || popcornResponse.ToUpper() == "N")
                                        {
                                            break;
                                        }
                                        else
                                        {
                                            Console.WriteLine("Please enter a valid response Y or N.");
                                        }
                                    }
                                    if (popcornResponse == "Y")
                                    {
                                        Adult adultTicket = new Adult(screening, true);
                                        order.AddTicket(adultTicket);
                                    }
                                    else if (popcornResponse == "N")
                                    {
                                        Adult adultTicket = new Adult(screening, false);
                                        order.AddTicket(adultTicket);
                                    }
                                }
                            }
                            if (ticketsNo - studentTicketsNo - seniorCitizenTicketsNo - adultTicketsNo == 0)
                            {
                                double amountPayable = 0.00;
                                int ticketsOrdered = order.TicketList.Count;
                                screening.SeatsRemaining -= ticketsOrdered;
                                foreach (Ticket ticket in order.TicketList)
                                {
                                    if (ticket is Student)
                                    {
                                        Student studentTicket = (Student)ticket;
                                        amountPayable += studentTicket.CalculatePrice();
                                    }
                                    else if (ticket is SeniorCitizen)
                                    {
                                        SeniorCitizen seniorCitizenTicket = (SeniorCitizen)ticket;
                                        amountPayable += seniorCitizenTicket.CalculatePrice();
                                    }
                                    else
                                    {
                                        Adult adultTicket = (Adult)ticket;
                                        amountPayable += adultTicket.CalculatePrice();
                                        if (adultTicket.PopcornOffer == true)
                                        {
                                            amountPayable += 3;
                                        }
                                    }
                                }
                                Console.WriteLine($"Amount Payable: ${amountPayable:0.00}");
                                order.Amount = amountPayable;
                                Console.Write("Press any key to make payment: ");
                                Console.ReadLine();
                                order.Status = "paid";
                                Console.WriteLine("Payment is successful.");
                                orderList.Add(order);

                            }
                            else
                            {
                                Console.WriteLine("The total number of student,senior citizen, adult tickets added up is not equivalent to the total number of tickets ordererd.");
                            }
                        }
                        else
                        {
                            Console.WriteLine("Please ensure that all the ticket holders meet movie classifications requirement.");
                        }

                    }
                    else
                    {
                        Order order = new Order(orderNo, DateTime.Now);
                        order.Status = "Unpaid";
                        orderNo += 1;
                        int studentTicketsNo = 0;
                        int seniorCitizenTicketsNo = 0;
                        int adultTicketsNo = 0;
                        while (true)
                        {
                            try
                            {
                                Console.Write("Enter the total number of student tickets to order: ");
                                studentTicketsNo = Convert.ToInt32(Console.ReadLine());
                                break;
                            }
                            catch (FormatException)
                            {
                                Console.WriteLine("Please enter a valid ticket no.");
                            }
                            catch (OverflowException)
                            {
                                Console.WriteLine("Please enter a valid ticket no.");
                            }
                        }
                        if (studentTicketsNo >= 1 && studentTicketsNo <= ticketsNo)
                        {
                            for (int i = 1; i <= studentTicketsNo; i++)
                            {
                                string levelOfStudy;
                                while (true)
                                {
                                    Console.Write($"{i}. Level of study[Primary,Secondary,Tertiary]: ");
                                    levelOfStudy = Console.ReadLine();
                                    if (levelOfStudy.ToLower() == "primary" || levelOfStudy.ToLower() == "secondary" || levelOfStudy.ToLower() == "tertiary")
                                    {
                                        break;
                                    }
                                    else
                                    {
                                        Console.WriteLine("Please enter a valid level of study.");
                                    }

                                }
                                Student studentTicket = new Student(screening, levelOfStudy);
                                order.AddTicket(studentTicket);
                            }
                        }
                        if (ticketsNo - studentTicketsNo != 0 && ticketsNo - studentTicketsNo > 0)
                        {
                            while (true)
                            {
                                try
                                {
                                    Console.Write("Enter the total number of senior citizen tickets to order: ");
                                    seniorCitizenTicketsNo = Convert.ToInt32(Console.ReadLine());
                                    break;
                                }
                                catch (FormatException)
                                {
                                    Console.WriteLine("Please enter a valid ticket no.");
                                }
                                catch (OverflowException)
                                {
                                    Console.WriteLine("Please enter a valid ticket no.");
                                }
                            }
                            if (seniorCitizenTicketsNo >= 1 && seniorCitizenTicketsNo <= ticketsNo - studentTicketsNo)
                            {
                                int yearOfBirth;
                                for (int t = 1; t <= seniorCitizenTicketsNo; t++)
                                {
                                    while (true)
                                    {
                                        try
                                        {
                                            Console.Write($"{t}.Year Of Birth: ");
                                            yearOfBirth = Convert.ToInt32(Console.ReadLine());
                                            if (yearOfBirth.ToString().Length == 4)
                                            {
                                                if (DateTime.Now.Year - yearOfBirth >= 55)
                                                {
                                                    SeniorCitizen seniorCitizenTicket = new SeniorCitizen(screening, yearOfBirth);
                                                    order.AddTicket(seniorCitizenTicket);
                                                    break;
                                                }
                                                else
                                                {
                                                    Console.WriteLine("The ticket holder must be 55 and above to buy seniorcitizen ticket. ");

                                                }
                                            }
                                            else
                                            {
                                                Console.WriteLine("Please enter a valid year made up of 4 integers.");
                                            }

                                        }
                                        catch (FormatException)
                                        {
                                            Console.WriteLine("Please enter a valid year made up of 4 integers.");
                                        }
                                        catch (OverflowException)
                                        {
                                            Console.WriteLine("Please enter a valid year made up of 4 integers.");
                                        }
                                    }

                                }
                            }
                        }
                        if (ticketsNo - studentTicketsNo - seniorCitizenTicketsNo != 0 && ticketsNo - studentTicketsNo - seniorCitizenTicketsNo > 0)
                        {
                            adultTicketsNo = ticketsNo - studentTicketsNo - seniorCitizenTicketsNo;
                            Console.WriteLine($"Number of adult tickets: {adultTicketsNo}");
                            string popcornResponse;

                            for (int y = 1; y <= adultTicketsNo; y++)
                            {
                                while (true)
                                {
                                    Console.Write($"{y}. Popcorn for $3 [Y/N]: ");
                                    popcornResponse = Console.ReadLine();
                                    if (popcornResponse.ToUpper() == "Y" || popcornResponse.ToUpper() == "N")
                                    {
                                        break;
                                    }
                                    else
                                    {
                                        Console.WriteLine("Please enter a valid response Y or N.");
                                    }
                                }

                                if (popcornResponse == "Y")
                                {
                                    Adult adultTicket = new Adult(screening, true);
                                    order.AddTicket(adultTicket);
                                }
                                else if (popcornResponse == "N")
                                {
                                    Adult adultTicket = new Adult(screening, false);
                                    order.AddTicket(adultTicket);
                                }

                            }

                        }
                        if (ticketsNo - studentTicketsNo - seniorCitizenTicketsNo - adultTicketsNo == 0)
                        {
                            double amountPayable = 0.00;
                            int ticketsOrdered = order.TicketList.Count;
                            screening.SeatsRemaining -= ticketsOrdered;
                            foreach (Ticket ticket in order.TicketList)
                            {
                                if (ticket is Student)
                                {
                                    Student studentTicket = (Student)ticket;
                                    amountPayable += studentTicket.CalculatePrice();
                                }
                                else if (ticket is SeniorCitizen)
                                {
                                    SeniorCitizen seniorCitizenTicket = (SeniorCitizen)ticket;
                                    amountPayable += seniorCitizenTicket.CalculatePrice();
                                }
                                else
                                {
                                    Adult adultTicket = (Adult)ticket;
                                    amountPayable += adultTicket.CalculatePrice();
                                    if (adultTicket.PopcornOffer == true)
                                    {
                                        amountPayable += 3;
                                    }
                                }
                            }
                            Console.WriteLine($"Amount Payable: ${amountPayable:0.00}");
                            order.Amount = amountPayable;
                            Console.Write("Press any key to make payment: ");
                            Console.ReadLine();
                            order.Status = "paid";
                            Console.WriteLine("Payment is successful.");
                            orderList.Add(order);

                        }
                        else
                        {
                            Console.WriteLine("The total number of student,senior citizen, adult tickets added up is not equivalent to the total number of tickets ordererd.");
                        }

                    }
                }
            }
            else
            {
                Console.WriteLine("Please enter a movie that has an ongoing screening.");
            }
        }
        static void DisplayOrder(List<Order> orderList)
        {
            Console.WriteLine("{0,-20}{1,-25}{2,-20}{3,-20}{4,-20}{5,-25}{6,-15}", "OrderNo", "ScreeningNo", "MovieName","TicketNo", "BookStatus","ScreeningDateTime","Total amount");
            foreach(Order order in orderList)
            {
                Console.WriteLine("{0,-20}{1,-25}{2,-20}{3,-20}{4,-20}{5,-25}{6,-15}", order.OrderNo,order.TicketList[0].Screening.ScreeningNo,order.TicketList[0].Screening.Movie.Title, order.TicketList.Count, order.Status,order.TicketList[0].Screening.ScreeningDateTime,"$"+order.Amount);
            }
        }
        static void CancelOrder(List<Movie> movieList, List<Order> orderList,List<Screening> screeningList)
        {
            bool validOrder = false;
            Order orderToCancel = new Order();
            Screening screeningAffected = new Screening();
            if (orderList.Count >= 1)
            {
                try
                {
                    Console.Write("Enter order number: ");
                    int orderNumber = Convert.ToInt32(Console.ReadLine());
                    foreach (Order order in orderList)
                    {
                        if (orderNumber == order.OrderNo)
                        {
                            orderToCancel = order;
                            validOrder = true;
                        }
                    }

                }
                catch (FormatException)
                {
                    Console.WriteLine("Please enter a valid order number made up of integer/s.");
                }
                catch (OverflowException)
                {
                    Console.WriteLine("Please enter a valid order number made up of integer/s.");
                }
                if (validOrder == true)
                {
                    bool screeningScreened = false;
                    foreach (Ticket ticket in orderToCancel.TicketList)
                    {
                        foreach (Screening screening in screeningList)
                        {
                            if (ticket.Screening.ScreeningNo == screening.ScreeningNo)
                            {
                                screeningAffected = screening;
                            }
                        }
                    }
                    if (screeningAffected.ScreeningDateTime <= DateTime.Now)
                    {
                        screeningScreened = true;
                    }
                    if (screeningScreened == false)
                    {
                        if (orderToCancel.Status != "Cancelled")
                        {
                            int ticketsNo = orderToCancel.TicketList.Count;
                            screeningAffected.SeatsRemaining += ticketsNo;
                            orderToCancel.Status = "Cancelled";
                            Console.WriteLine($"${orderToCancel.Amount:0.00} has been successfully refunded.");
                            Console.WriteLine("The cancelation of order is successful.");

                        }
                        else
                        {
                            Console.WriteLine("The order has already been cancelled before.");
                            Console.WriteLine("The cancelation of order is unsuccessful.");
                        }
                    }
                    else
                    {
                        Console.WriteLine("The screening has already been screened and cannot be cancelled.");
                        Console.WriteLine("The cancelation of order is unsuccessful.");
                    }
                }
                else
                {
                    Console.WriteLine("The order cannot be found.");
                    Console.WriteLine("The cancelation of order is unsuccessful.");
                }

            }
            else
            {
                Console.WriteLine("There are currently no orders to be cancelled.");
            }
            
            

        }
        //3.1 Advanced Feature (Recommend movie based on sale of tickets sold)
        static void RecommendMovie(List<Movie> movieList,List<Order> orderList)
        {
            List<string> movieNamesList = new List<string>();
            List<int> countTicketList = new List<int>();
            foreach(Movie movie in movieList)
            {
                movieNamesList.Add(movie.Title);
                countTicketList.Add(0);
            }
            foreach(Order order in orderList)
            {
                if (order.Status != "Cancelled")
                {
                    foreach (Ticket ticket in order.TicketList)
                    {
                        foreach (string movieName in movieNamesList)
                        {
                            if (movieName == ticket.Screening.Movie.Title)
                            {
                                countTicketList[movieNamesList.IndexOf(movieName)] += 1;
                            }
                        }

                    }
                }              
            }
            int highestTicketCount = 0;
            foreach(int ticketCount in countTicketList)
            {
                if (ticketCount > highestTicketCount)
                {
                    highestTicketCount = ticketCount;
                }
            }
            if (highestTicketCount > 0)
            {
                Console.WriteLine($"Our recommended movie is {movieNamesList[countTicketList.IndexOf(highestTicketCount)]}");
            }
            else
            {
                Console.WriteLine("Currently do not have any movies to recommend.");
            }
            


        }
        //3.2 Display available seats of screening session in descending order
        static void DisplayAvailableSeats(List<Screening> screeningList)
        {
            screeningList.Sort();
            Console.WriteLine("{0,-15}{1,-30}{2,-15}{3,-15}{4,-15}{5,-15}", "ScreeningNo", "ScreeningDateTime", "ScreeningType", "SeatsRemaining","CinemaName","CinemaHall");
            foreach (Screening screening in screeningList)
            {
                Console.WriteLine("{0,-15}{1,-30}{2,-15}{3,-15}{4,-15}{5,-15}", screening.ScreeningNo, screening.ScreeningDateTime, screening.ScreeningType, screening.SeatsRemaining,screening.Cinema.Name,screening.Cinema.HallNo);
            }
        }
    }
}
